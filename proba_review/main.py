from fastapi import FastAPI

from proba_review.bert import text_clas
from proba_review.models import RequestsReview, ResponseReview

app = FastAPI()


@app.post('/prob', response_model=ResponseReview)
async def prob(request: RequestsReview):
    result = text_clas(request.text)
    return ResponseReview(
        review_id=request.review_id,
        status=result[0]['label'],
        prob=result[0]['score'],

    )

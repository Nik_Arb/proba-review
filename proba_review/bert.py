from transformers import (AutoModelForSequenceClassification,
                          BertTokenizerFast, pipeline)

tokenizer = BertTokenizerFast.from_pretrained('blanchefort/rubert-base-cased-sentiment')
model = AutoModelForSequenceClassification.from_pretrained('blanchefort/rubert-base-cased-sentiment', return_dict=True)

def text_clas(text):
    pipe = pipeline("text-classification", model=model, tokenizer=tokenizer)
    return pipe(text)

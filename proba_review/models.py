from typing import List, Optional

from pydantic import BaseModel, Field, validator


class Images(BaseModel):
    r: List[int]
    g: List[int]
    b: List[int]


class RequestsReview(BaseModel):
    review_id: int
    text: str = Field(
        max_length=1000,
        description='review',

    )
    images: Optional[List[str]] = None

    @validator('text')
    def lowerCase(cls, v: str) -> str:
        return v.lower()


class ResponseReview(BaseModel):
    review_id: int
    status: str
    prob: float

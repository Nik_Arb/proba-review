#!/bin/bash

# env name
ENV_NAME=`basename "$PWD"`

# conda
conda activate base
conda env remove -y -n $ENV_NAME
conda create -y -n $ENV_NAME python=3.9
conda activate $ENV_NAME

# poetry
export POETRY_HOME="$PWD/poetry"
export POETRY_VERSION="1.5.1"

curl -sSL https://install.python-poetry.org | python3.9 - --version $POETRY_VERSION

poetry install

# clean
unset ENV_NAME
FROM python:3.9-slim


WORKDIR /proba_review

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV POETRY_VERSION "1.5.1"
ENV POETRY_HOME "/etc/poetry/"

RUN apt-get update \
    && apt-get install curl -y \
    && curl -sSL https://install.python-poetry.org | python3.9 - --version $POETRY_VERSION

ENV PATH="$POETRY_HOME/bin:$PATH"

EXPOSE 8000

COPY . .

RUN poetry config virtualenvs.create false \
    && poetry install --no-dev --no-interaction --no-ansi

CMD ["uvicorn", "proba_review.main:app", "--host", "0.0.0.0", "--port", "8000"]
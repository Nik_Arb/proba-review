from proba_review.main import text_clas


def test_a():
    assert text_clas('плохо')[0]['label'] == "NEGATIVE"

def test_b():
    assert text_clas('хорошо')[0]['label'] == "POSITIVE"

def test_c():
    type(text_clas('и что у нас по вероятностям')[0]['score']) == float
